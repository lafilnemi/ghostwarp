_addon.name = 'ghostwarp'
_addon.author = 'Nemi'
_addon.version = '0.97.2'
_addon.commands = {'gw', 'wa'}
require('tables')
require('logger')
require('functions')
require('coroutine')
config = require('config')

require('fuzzyfind')
require('helpers')
require('homepoints')
require('sguides')
require('unity')

local warp_buffer = 0
local warp_delay = 10
local warp_doeet = false
local warp_destination = ''
local warp_offset = 1
local warp_cmd = ''
local warp_npc = nil
local warp_npcindex = nil
local warp_zoneindex = nil
local warp_open_menu_count = 0

function warp_command(cmd, args)
	warp_clear()
	windower.add_to_chat(55,"COMMAND: "..args:concat(' '))
	windower.send_command(warp_escape()..';input /targetnpc')
	warp_delay = 0
	warp_offset = getOffset(args)
	warp_destination = args:concat(' ')
	warp_cmd = cmd:lower()
	if warp_offset == nil then
		warp_offset = '1'
	end
	warp_doeet = true
end

function getOffset(args) 
	local offset 
	if tonumber(tonumber(args[#args], 10)) then
		offset = args[#args]
		args:remove(#args)
		return offset
	end
	return nil
end

function warp_ticker()
	warp_buffer = warp_buffer + 1
	
	if warp_buffer - math.floor(warp_buffer/50)*50 ~= 0 then
		return
	end
	warp_open_menu_count = warp_open_menu_count + 1
	if warp_open_menu_count == 5 and warp_cmd ~= '' then
		windower.add_to_chat(55,'MENU DIDNT OPEN RETRYING')
		windower.send_command(warp_escape()..';input /targetnpc')
		warp_doeet = true
		warp_delay = 0
	end
	
	if not warp_doeet or warp_delay > 5 then 
		return
	end
	local info = windower.ffxi.get_info();
	warp_zoneindex = info.zone
	local index = windower.ffxi.get_player().target_index
	local target = windower.ffxi.get_mob_by_index(index or 0)

	if target and target.name and (
		string.find(target.name ,'Home Point') 
		or string.find(target.name ,'Survival Guide')
		or string.find(target.name ,'Teldro')
		or string.find(target.name ,'Urbiolaine')
		or string.find(target.name ,'Igsli')
	) then
		windower.add_to_chat(55,'FOUND NPC WITH INDEX ['..target.index..'] ID ['..target.id..']')
		warp_npcindex = target.index
		warp_npc = target.id
		if warp_cmd == 'hp' then
			open_warp_menu()
		elseif warp_cmd == 'sg' then
			open_sguide_menu()
		elseif warp_cmd == 'u' then
			open_unity_menu()
		end
		warp_doeet = false
	else
		windower.add_to_chat(55,'POINT NOT FOUND RETRYING '..warp_delay)
		warp_delay = warp_delay + 1
		windower.send_command('input /targetnpc')
	end
end

function warp_clear()
	warp_destination = ''
	warp_doeet = false
	warp_offset = '1'
	warp_cmd = ''
	warp_npc = nil
	warp_npcindex = nil
	warp_zoneindex = nil
end


-- HP WARPS

local oneHPZones = T{
	[113] = 'Cape Teriggan',
	[249] = 'Mhaura',
	[205] = 'Ifrit\'s Cauldron',
	[94] = 'Windurst Waters \[S\]',
	[80] = 'Southern San d\'Oria \[S\]',
	[87] = 'Bastok Markets \[S\]',
	[7] = 'Attohwa Chasm',
	[61] = 'Mount Zhayolm',
	[248] = 'Selbina',
} 

function open_warp_menu()
	warp_open_menu_count = 0
	windower.send_command(warp_menu())
end

function do_warp(dest, offset, zoneIndex)
	windower.add_to_chat(55,'TRYING TO WARP TO: '..dest..' - '..offset)
	windower.add_to_chat(55,'CURRENTLY IN ZONE '..zoneIndex)
	
	local closest_zone_name, closest_zone_value = fmatch(dest, get_hp_keys())
	
	if closest_zone_name then
		windower.add_to_chat(55,"HP FOUND: "..closest_zone_name..' '..offset)
						
		if hp_warpdata[closest_zone_name][offset] then
			local command = 'wait 3;'..warp_enter()..warp_wait_semi()
			local region = hp_warpdata[closest_zone_name]['region']
			local zone = hp_warpdata[closest_zone_name]['zone']
			if oneHPZones[zoneIndex] then
				local currentZone = hp_warpdata[oneHPZones[zoneIndex]]
				windower.add_to_chat(55,'FOUND SPECIAL ZONE '..zone)
				windower.add_to_chat(55,'SUBSTRACTING CURRENT REGION > -1')
				region = region-1
				if hp_warpdata[closest_zone_name]['page'] == currentZone['page'] and hp_warpdata[closest_zone_name]['region'] > currentZone['region'] then
					windower.add_to_chat(55,'NEW REGION LOWER > -1')
					region = region-1
				end
				if hp_warpdata[closest_zone_name]['region'] == currentZone['region'] and zone > currentZone['zone'] then
					windower.add_to_chat(55,'NEW ZONE LOWER > -1')
					zone = zone-1
				end
			end
			if hp_warpdata[closest_zone_name]['page'] == 2 then
				command = command..warp_menu_page_two()
			end
			command = command..hp_navigate_region(region)..hp_navigate_zone(zone)..hp_navigate_point(offset)
			
			windower.send_command('keyboard_blockinput 1;'..command..'keyboard_blockinput 0')
		else
			windower.add_to_chat(55,'OFFSET NOT DEFINED')
		end
	else 
		windower.add_to_chat(55,"HP NOT FOUND: "..dest)
	end
end

function open_sguide_menu()
	warp_open_menu_count = 0
	windower.send_command(sg_menu())
end

function do_sguide(dest, offset, zoneIndex)
	windower.add_to_chat(55,'TRYING TO SG WARP TO: '..dest)
	
	local closest_zone_name, closest_zone_value = fmatch(dest, get_sg_keys())
	
	if closest_zone_name then
		windower.add_to_chat(55,"SG FOUND: "..closest_zone_name)
		if sg_warpdata[closest_zone_name] then
			local command = 'wait 2;'..sg_menu()
			local region = sg_warpdata[closest_zone_name]['region']
			local zone = sg_warpdata[closest_zone_name]['zone']
			if sg_warpdata[closest_zone_name]['page'] == 2 then
				command = command..sg_menu_page_two()
			end
			command = command..sg_navigate_region(region)..sg_navigate_zone(zone)..sg_confirm()
			windower.send_command('keyboard_blockinput 1;'..command..'keyboard_blockinput 0')
		else
			windower.add_to_chat(55,'OFFSET NOT DEFINED')
		end
	else 
		windower.add_to_chat(55,"SG NOT FOUND: "..dest)
	end
end

function open_unity_menu()
	warp_open_menu_count = 0
	windower.send_command(u_menu())
end

function do_uguide(dest, offset, zoneIndex)
	windower.add_to_chat(55,'TRYING TO UNITY WARP TO: '..dest)
	
	local closest_zone_name, closest_zone_value = fmatch(dest, get_u_keys())
	
	if closest_zone_name then
		windower.add_to_chat(55,"UNITY FOUND: "..closest_zone_name)
		if u_warpdata[closest_zone_name] then
			local command = 'wait 2;'..u_to_levels()
			local region = u_warpdata[closest_zone_name]['level']
			local zone = u_warpdata[closest_zone_name]['zone']
			command = command..u_navigate_region(region)
			if u_warpdata[closest_zone_name]['page'] == 2 then
				command = command..u_menu_page_two()
			end
			command = command..u_navigate_zone(zone)..u_confirm()
			windower.send_command('keyboard_blockinput 1;'..command..'keyboard_blockinput 0')
		else
			windower.add_to_chat(55,'OFFSET NOT DEFINED')
		end
	else 
		windower.add_to_chat(55,"SG NOT FOUND: "..dest)
	end
end


function get_hp_keys()
	local keyset={}
	local n=0

	for k,v in pairs(hp_warpdata) do
	  n=n+1
	  keyset[n]=k
	end
	return keyset
end
function get_sg_keys()
	local keyset={}
	local n=0
	for k,v in pairs(sg_warpdata) do
	  n=n+1
	  keyset[n]=k
	end
	return keyset
end
function get_u_keys()
	local keyset={}
	local n=0
	for k,v in pairs(u_warpdata) do
	  n=n+1
	  keyset[n]=k
	end
	return keyset
end

windower.register_event('prerender', warp_ticker)

windower.register_event('addon command', function(...)
    local args = T{...}
    local cmd = args[1]
	for i,v in pairs(args) do args[i]=windower.convert_auto_trans(args[i]) end
	local all = S{'all','a','@all'}:contains(args[2]:lower())
    if all then 
        args:remove(2) 
		windower.add_to_chat(55,'SENDING TO ALL: '..args:concat(' '))
        windower.send_command('send @all wa '..args:concat(' '))
        return
    end
    if cmd:lower() == 'hp' or cmd:lower() == 'sg' or cmd:lower() == 'u' then
		args:remove(1)
        warp_command(cmd:lower(), args)
    end
end)


packets = require('packets')

windower.register_event('incoming chunk',function(id,data,modified,injected,blocked)

    if id == 0x034 or id == 0x032 then
        local p = packets.parse('incoming', data)
        last_menu = p["Menu ID"]
        last_npc = p["NPC"]
        last_npc_index = p["NPC Index"]
		if warp_destination ~= '' and warp_cmd ~= '' and warp_npcindex ~= nil then
			warp_open_menu_count = 100
			windower.add_to_chat(55,'Menu ID ['..last_menu..'] NPC ['..last_npc..'] NPC Index ['..last_npc_index..']  warp_cmd ['..warp_cmd..']  warp_npc ['..warp_npc..']  warp_npcindex ['..warp_npcindex..'] ')
			if warp_cmd == 'hp' and last_npc == warp_npc and last_npc_index == warp_npcindex then 
				do_warp(warp_destination, warp_offset, warp_zoneindex)
			end
			if warp_cmd == 'sg' then
				do_sguide(warp_destination, warp_offset, warp_zoneindex)
			end
			if warp_cmd == 'u' then
				do_uguide(warp_destination, warp_offset, warp_zoneindex)
			end
			warp_clear()
			--8701 Home Point MAIN WARP MENU
			--8501 Survival guide MAIN MENU
		end
	end

end)



