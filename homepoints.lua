function warp_menu()
	return warp_enter()..warp_wait_semi()..warp_enter()
end	


function warp_menu_page_two() 
	return right_x(9)..menu_confirm()
end

function warp_to_x(offset)
	return down_x(offset)..warp_enter()..warp_wait_semi()..warp_confirm()
end
function warp_confirm()
	return warp_up()..warp_wait_short()..warp_enter()
end

function hp_navigate_region(offset)
	return down_x(offset)..menu_confirm()
end
function hp_navigate_zone(offset)
	return down_x(offset)..menu_confirm()
end
function hp_navigate_point(offset)
	return warp_to_x(offset)
end

-- HP END HERE
hp_warpdata = T{
		['Southern San d\'Oria'] = {
			['page'] = 1,
			['region'] = 3,
			['zone'] = 1,
            ['1'] = 1,
			['2'] = 2,
			['3'] = 3,
			['4'] = 4,
		},
        ['Northern San d\'Oria'] = {
			['page'] = 1,
			['region'] = 3,
			['zone'] = 2,
            ['1'] = 1,
			['2'] = 2,
			['3'] = 3,
			['4'] = 4,
		},
        ['Port San d\'Oria'] = {
			['page'] = 1,
			['region'] = 3,
			['zone'] = 3,
            ['1'] = 1,
			['2'] = 2,
			['3'] = 3,
		},
        ['Bastok Mines'] = {
			['page'] = 1,
			['region'] = 4,
			['zone'] = 1,
            ['1'] = 1,
			['2'] = 2,
			['3'] = 3,
		},
        ['Bastok Markets'] = {
			['page'] = 1,
			['region'] = 4,
			['zone'] = 2,
            ['1'] = 1,
			['2'] = 2,
			['3'] = 3,
			['4'] = 4,
		},
        ['Port Bastok'] = {
			['page'] = 1,
			['region'] = 4,
			['zone'] = 3,
            ['1'] = 1,
			['2'] = 2,
			['3'] = 3,
		},
        ['Metalworks'] = {
			['page'] = 1,
			['region'] = 4,
			['zone'] = 4,
            ['1'] = 1,
			['2'] = 2,
		},
        ['Windurst Waters'] = {
			['page'] = 1,
			['region'] = 5,
			['zone'] = 1,
			['1'] = 1,
			['2'] = 2,
			['3'] = 3,
			['4'] = 4,
		},
        ['Windurst Walls'] = {
			['zone'] = 2,
			['page'] = 1,
			['region'] = 5,
			['1'] = 1,
			['2'] = 2,
			['3'] = 3,
		},
        ['Port Windurst'] = {
			['page'] = 1,
			['region'] = 5,
			['zone'] = 3,
			['1'] = 1,
			['2'] = 2,
			['3'] = 3,
		},
        ['Windurst Woods'] = {
			['page'] = 1,
			['zone'] = 4,
			['region'] = 5,
			['1'] = 1,
			['2'] = 2,
			['3'] = 3,
			['4'] = 4,
			['5'] = 5,
		},
        ['Ru\'Lude Gardens'] = {
			['page'] = 1,
			['region'] = 6,
			['zone'] = 1,
			['1'] = 1,
			['2'] = 2,
			['3'] = 3,
        },
		['Upper Jeuno'] = {
			['page'] = 1,
			['region'] = 6,
			['zone'] = 2,
			['1'] = 1,
			['2'] = 2,
			['3'] = 3,
        },
		['Lower Jeuno'] = {
			['page'] = 1,
			['region'] = 6,
			['zone'] = 3,
			['1'] = 1,
			['2'] = 2,
		},
		['Port Jeuno'] = {
			['page'] = 1,
			['region'] = 6,
			['zone'] = 4,
			['1'] = 1,
			['2'] = 2,
		},
        ['Kazham'] = {
			['page'] = 1,
			['region'] = 22,
			['zone'] = 1, 
			['1'] = 1,
		},
        ['Mhaura'] = {
			['page'] = 1,
			['region'] = 14,
			['zone'] = 1, 
			['1'] = 1,
		},
        ['Norg'] = {
			['page'] = 1,
			['region'] = 22,
			['zone'] = 2,
            ['1'] = 1,
			['2'] = 2,
		},
        ['Rabao'] = {
			['page'] = 1,
			['region'] = 20,
			['zone'] = 2,
            ['1'] = 1,
			['2'] = 2,
		},
        ['Selbina'] = {
			['page'] = 1,
			['region'] = 11,
			['zone'] = 1,
			['1'] = 1,
		},
        ['Western Adoulin'] = {
			['page'] = 1,
			['region'] = 9,
			['zone'] = 1,
			['1'] = 1,
			['2'] = 2,
		},
        ['Eastern Adoulin'] = {
			['page'] = 1,
			['region'] = 9,
			['zone'] = 2,
            ['1'] = 1,
			['2'] = 2,
		},
        ['Fei\'Yin'] = {
			['page'] = 1,
			['region'] = 16,
			['zone'] = 2,
            ['1'] = 1,
			['2'] = 2,
		},
        ['Quicksand Caves'] = {
			['page'] = 1,
			['region'] = 20,
			['zone'] = 1,
            ['1'] = 1,
			['2'] = 2,
		},
        ['Den of Rancor'] = {
			['page'] = 1,
			['region'] = 23,
			['zone'] = 1,
            ['1'] = 1,
			['2'] = 2,
		},
        ['Castle Zvahl Keep'] = {
			['page'] = 1,
			['region'] = 17,
			['zone'] = 2, 
            ['1'] = 1,
		},
        ['Ru\'Aun Gardens'] = {
			['page'] = 1,
			['region'] = 24,
			['zone'] = 1,
            ['1'] = 1,
			['2'] = 2,
			['3'] = 3,
			['4'] = 4,
			['5'] = 5,
			['6'] = 6,
		},
        ['Tavnazian Safehold'] = {
			['page'] = 1,
			['region'] = 7,
			['zone'] = 1,
            ['1'] = 1,
			['2'] = 2,
			['3'] = 3,
		},
        ['Aht Urhgan Whitegate'] = {
			['page'] = 1,
			['region'] = 8,
			['zone'] = 1,
		    ['1'] = 1,
			['2'] = 2,
			['3'] = 3,
			['4'] = 4,
		},
		['Misareaux Coast'] = {
			['page'] = 2,
			['region'] = 4,
			['zone'] = 1, 
			['1'] = 1,
		},
		['Ceizak Battlegrounds'] = {
			['page'] = 2,
			['region'] = 12,
			['zone'] = 1,
			['1'] = 1,
			},
		['Foret de Hennetiel'] = {
			['page'] = 2,
			['region'] = 12,
			['zone'] = 2,
			['1'] = 1,
		},
		['Morimar Basalt Fields'] = {
			['page'] = 2,
			['region'] = 12,
			['zone'] = 4,
			['1'] = 1,
		},
		['Yorcia Weald'] = {
			['page'] = 2,
			['region'] = 12,
			['zone'] = 3,
			['1'] = 1,
		},
		['Marjami Ravine'] = {
			['page'] = 2,
			['region'] = 12,
			['zone'] = 5, 
			['1'] = 1,
		},
		['Kamihr Drifts'] = {
			['page'] = 2,
			['region'] = 12,
			['zone'] = 6, 
			['1'] = 1,
		},
		['Leafallia'] = {
			['page'] = 2,
			['region'] = 13,
			['zone'] = 6,
			['1'] = 1,
		},
		['Yughott Grotto'] = {
			['page'] = 1,
			['region'] = 10,
			['zone'] = 1, 
			['1'] = 1,
		},
		['Palborough Mines'] = {
			['page'] = 1,
			['region'] = 12,
			['zone'] = 1, 
			['1'] = 1,
		},
		['Giddeus'] = {
			['page'] = 1,
			['region'] = 12,
			['zone'] = 1,
			['1'] = 1,
		},
		['Upper Delkfutt\'s Tower'] = {
			['page'] = 1,
			['region'] = 18,
			['zone'] = 2,
			['1'] = 1,
		},
		['The Shrine of Ru\'Avitau'] = {
			['page'] = 1,
			['region'] = 24,
			['zone'] = 2,
			['1'] = 1,
		},
		['Riverne - Site \#B01'] = {
			['page'] = 2,
			['region'] = 4,
			['zone'] = 3,
			['1'] = 1,
		},
		['Bhaflau Thickets'] = {
			['page'] = 1,
			['region'] = 8,
			['zone'] = 2,
			['1'] = 1,
		},
		['Caedarva Mire'] = {
			['page'] = 2,
			['region'] = 7,
			['zone'] = 2,
			['1'] = 1,
		},
		['Uleguerand Range'] = {
			['page'] = 1,
			['region'] = 17,
			['zone'] = 1,
			['1'] = 1,
			['2'] = 2,
			['3'] = 3,
			['4'] = 4,
			['5'] = 5,
		},
		['Attohwa Chasm'] = {
			['page'] = 1,
			['region'] = 15,
			['zone'] = 1,
			['1'] = 1,
		},
		['Pso\'Xja'] = {
			['page'] = 1,
			['region'] = 16,
			['zone'] = 1,
			['1'] = 1,
		},
		['Newton Movalpolos'] = {
			['page'] = 1,
			['region'] = 3,
			['zone'] = 1,
			['1'] = 1,
		},
		['Riverne - Site #A01'] = {
			['page'] = 2,
			['region'] = 4,
			['zone'] = 2,
			['1'] = 1,
		},
		['Al\'Taieu'] = {
			['page'] = 2,
			['region'] = 5,
			['zone'] = 1,
			['1'] = 1,
			['2'] = 2,
			['3'] = 3,
		},
		['Grand Palace of Hu\'Xzoi'] = {
			['page'] = 2,
			['region'] = 5,
			['zone'] = 2,
			['1'] = 1,
		},
		['The Garden of Ru\'Hmet'] = {
			['page'] = 2,
			['region'] = 5,
			['zone'] = 3,
			['1'] = 1,
		},
		['Mount Zhayolm'] = {
			['page'] = 2,
			['region'] = 6,
			['zone'] = 1,
			['1'] = 1,
		},
		['Cape Teriggan'] = {
			['page'] = 1,
			['region'] = 21,
			['zone'] = 1,
			['1'] = 1,
		},
		['The Boyahda Tree'] = {
			['page'] = 1,
			['region'] = 19,
			['zone'] = 1,
			['1'] = 1,
		},
		['Ifrit\'s Cauldron'] = {
			['page'] = 1,
			['region'] = 23,
			['zone'] = 2,
			['1'] = 1,
		},
		['Xarcabard \[S\]'] = {
			['page'] = 2,
			['region'] = 11,
			['zone'] = 1,
			['1'] = 1,
		},
		
		['Castle Zvahl Keep \[S\]'] = {
			['page'] = 1,
			['region'] = 17,
			['zone'] = 2,
			['1'] = 1,
		},
		['Qufim Island'] = {
			['page'] = 1,
			['region'] = 18,
			['zone'] = 1,
			['1'] = 1,
		},
		['Toraimarai Canal'] = {
			['page'] = 1,
			['region'] = 13,
			['zone'] = 2,
			['1'] = 1,
		},
		['Ra\'Kaznar Inner Court'] = {
			['page'] = 2,
			['region'] = 13,
			['zone'] = 1,
			['1'] = 1,
		},
		['Nashmau'] = {
			['page'] = 2,
			['region'] = 7,
			['zone'] = 1,
			['1'] = 1,
		},
		['Windurst Waters \[S\]'] = {
			['page'] = 2,
			['region'] = 10,
			['zone'] = 1,
			['1'] = 1,
		},
		['Southern San d\'Oria \[S\]'] = {
			['page'] = 2,
			['region'] = 8,
			['zone'] = 1,
            ['1'] = 1,
		},
		['Bastok Markets \[S\]'] = {
			['page'] = 2,
			['region'] = 9,
			['zone'] = 1,
            ['1'] = 1,
		},
}

