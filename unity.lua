function u_menu()
	return warp_enter()..warp_wait_semi()..warp_enter()
end	

function u_to_levels()
	return down_x(1)..warp_enter()..warp_wait_long()
end	

function u_menu_page_two() 
	return right_x(12)..menu_confirm()
end

function u_navigate_region(offset)
	return down_x(offset)..menu_confirm()
end
function u_navigate_zone(offset)
	return down_x(offset)..menu_confirm()
end
function u_confirm()
	return up_x(1)..warp_wait_short()..warp_enter()
end

-- HP END HERE
u_warpdata = T{
		--99
		['East Ronfaure'] = {
			['level'] = 3,
			['page'] = 1,
			['zone'] = 1,
		},
		['South Gustaberg'] = {
			['level'] = 3,
			['page'] = 1,
			['zone'] = 2,
		},
		['East Sarutabaruta'] = {
			['level'] = 3,
			['page'] = 1,
			['zone'] = 3,
		},
		['La Theine Plateau'] = {
			['level'] = 3,
			['page'] = 1,
			['zone'] = 4,
		},
		['Konstat Highlands'] = {
			['level'] = 3,
			['page'] = 1,
			['zone'] = 5,
		},
		['Tahrongi Canyon'] = {
			['level'] = 3,
			['page'] = 1,
			['zone'] = 6,
		},
		--119
		['Valkrum Dunes'] = {
			['level'] = 4,
			['page'] = 1,
			['zone'] = 1,
		},
		['Buburimu Peninsula'] = {
			['level'] = 4,
			['page'] = 1,
			['zone'] = 2,
		},
		['Qufim Island'] = {
			['level'] = 4,
			['page'] = 1,
			['zone'] = 3,
		},
		['Bibiki Bay'] = {
			['level'] = 3,
			['page'] = 1,
			['zone'] = 4,
		},
		["Carpenters' Landing"] = {
			['level'] = 4,
			['page'] = 1,
			['zone'] = 5,
		},
		['Yahtunga Jungle'] = {
			['level'] = 4,
			['page'] = 1,
			['zone'] = 6,
		},
		['Lufaise Meaows'] = {
			['level'] = 4,
			['page'] = 1,
			['zone'] = 7,
		},
		--125
		['Batallia Downs'] = {['level'] = 5,['page'] = 1,['zone'] = 1,},
		['Rolanberry Fields'] = {['level'] = 5,['page'] = 1,['zone'] = 2,},
		['Sauromugue Champaign'] = {['level'] = 5,['page'] = 1,['zone'] = 3,},
		['Beaucedine Glacier'] = {['level'] = 5,['page'] = 1,['zone'] = 4,},
		['Xarcabard'] = {['level'] = 5,['page'] = 1,['zone'] = 5,},
		["Ro'Maeve"] = {['level'] = 5,['page'] = 1,['zone'] = 6,},
		['Western Altepa Desert'] = {['level'] = 5,['page'] = 1,['zone'] = 7,},
		['Attohwa Chasm'] = {['level'] = 5,['page'] = 1,['zone'] = 8,},
		--p2
		['Garlaige Citadel'] = {['level'] = 5,['page'] = 2,['zone'] = 1,},
		["Ifrit's Cauldron"] = {['level'] = 5,['page'] = 2,['zone'] = 2,},
		['The Boyahda Tree'] = {['level'] = 5,['page'] = 2,['zone'] = 3,},
		['Kuftal Tunnel'] = {['level'] = 5,['page'] = 2,['zone'] = 4,},
		['Sea Serpent Grotto'] = {['level'] = 5,['page'] = 2,['zone'] = 5,},
		['Temple of Uggalepih'] = {['level'] = 5,['page'] = 2,['zone'] = 6,},
		['Quicksand Caves'] = {['level'] = 5,['page'] = 2,['zone'] = 7,},
		['Wjoam Woodlands'] = {['level'] = 5,['page'] = 2,['zone'] = 8,},
		--p3	
		['Lufaise Meadows'] = {['level'] = 5,['page'] = 3,['zone'] = 1,},
		
		--128
		['Cape Terrigan'] = {['level'] = 6,['page'] = 1,['zone'] = 1,},
		['Uleguerand Range'] = {['level'] = 6,['page'] = 1,['zone'] = 2,},
		['Den of Rancor'] = {['level'] = 6,['page'] = 1,['zone'] = 3,},
		["Fei'Yin"] = {['level'] = 6,['page'] = 1,['zone'] = 4,},
		['Misareaux Coast'] = {['level'] = 6,['page'] = 1,['zone'] = 5,},
		['Mount Zhayolm'] = {['level'] = 6,['page'] = 1,['zone'] = 6,},
		['Gustav Tunnel'] = {['level'] = 6,['page'] = 1,['zone'] = 7,},
		--135
		["Behemoth's Dominion"] = {['level'] = 7,['page'] = 1,['zone'] = 1,},
		["The Boyahda Tree2"] = {['level'] = 7,['page'] = 1,['zone'] = 2,},
		["Valley of Sorrows"] = {['level'] = 7,['page'] = 1,['zone'] = 3,},
		["Wajaom Woodlands2"] = {['level'] = 7,['page'] = 1,['zone'] = 4,},
		["Mount Zhayolm2"] = {['level'] = 7,['page'] = 1,['zone'] = 5,},

}

