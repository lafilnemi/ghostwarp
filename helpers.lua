function warp_wait_short()
	return 'wait .3;'
end
function warp_wait_vshort()
	return 'wait .2;'
end
function warp_wait_semi()
	return 'wait .4;'
end
function warp_wait_mid()
	return 'wait 1;'
end

function warp_wait_long()
	return 'wait 3;'
end

function warp_info(str)
	--windower.add_to_chat(12,str)
	return ''
end

function down_x(x)
	local str = ''
	local hop = math.floor(x/3);
	for i=1,hop do 
		str = str..warp_right()..warp_wait_short()
	end
	for i=1,(x-(hop*3)) do 
		str = str..warp_down()..warp_wait_short()
	end
	return str
end
function up_x(x)
	local str = ''
	for i=1,x do 
		str = str..warp_up()..warp_wait_short()
	end
	return str
end

function right_x(x)
	local str = ''
	for i=1,x do 
		str = str..warp_right()..warp_wait_short()
	end
	return str
end

function warp_enter()
	return ';setkey enter;'..warp_wait_semi()..'setkey enter up;'
end

function warp_up()
	return ';setkey up;'..warp_wait_vshort()..'setkey up up;'
end
function warp_down()
	return ';setkey down;'..warp_wait_vshort()..'setkey down up;'
end
function warp_right()
	return ';setkey right;'..warp_wait_vshort()..'setkey right up;'
end

function warp_escape()
	return ';setkey escape;'..warp_wait_vshort()..'setkey escape up;'
end



function menu_confirm()
	return warp_enter()..warp_wait_semi()
end
